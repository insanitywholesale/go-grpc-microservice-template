package main

import (
	"log"
	"net/http"
	"os"
	"time"

	front "gitlab.com/insanitywholesale/go-grpc-microservice-template/frontend"
)

func main() {
	backendAddr := os.Getenv("BACKEND_ADDRESS")

	if backendAddr == "" {
		backendName := os.Getenv("BACKEND_NAME")
		if backendName == "" {
			backendName = "localhost"
		}
		backendPort := os.Getenv("BACKEND_PORT")
		if backendPort == "" {
			backendPort = "15200"
		}
		backendAddr = backendName + ":" + backendPort
	}

	frontendHandler, err := front.CreateFrontendHandler(backendAddr)
	if err != nil {
		log.Fatal("Failed creating frontend handler:", err)
	}

	frontendPort := os.Getenv("FRONTEND_PORT")
	if frontendPort == "" {
		frontendPort = "8089"
	}

	srv := &http.Server{
		Addr:              ":" + frontendPort,
		Handler:           frontendHandler,
		ReadTimeout:       10 * time.Second,
		ReadHeaderTimeout: 5 * time.Second,
		WriteTimeout:      20 * time.Second,
		IdleTimeout:       30 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
