//go:build mage
// +build mage

package main

import (
	"os"

	"github.com/magefile/mage/sh"
)

func CheckFormat() error {
	return sh.Run("diff", "-u", "<(echo -n)", "<(gofmt -d ./)")
}

func Protos() error {
	err := sh.Run("buf", "generate", "--timeout=5m30s")
	if err != nil {
		dir, e := os.Getwd()
		if e != nil {
			return e
		}
		return sh.Run("docker", "run", "--rm",
			"-v", dir+":/src",
			"-w", "/src",
			"bufbuild/buf:latest", "generate", "--timeout=5m30s",
		)
	}
	return err
}

func GoFakeRelease() error {
	return sh.Run(
		"go install -v github.com/goreleaser/goreleaser@latest",
		"goreleaser --snapshot --skip-publish --rm-dist",
	)
}

func GoRelease() error {
	// Use this for now, in the future make it publish to GL and GH
	return GoFakeRelease()
}
