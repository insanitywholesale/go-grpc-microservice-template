package frontend

import (
	"context"
	"embed"
	"html/template"
	"io/fs"
	"net/http"

	"github.com/gorilla/mux"
	pb "gitlab.com/insanitywholesale/go-grpc-microservice-template/proto/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	//go:embed templates
	templates  embed.FS
	templatefs fs.FS
	hc         pb.HelloServiceClient
)

func CreateFrontendHandlerV1(backendAddr string) (http.Handler, error) {
	client, err := createGRPCClientV1(backendAddr)
	if err != nil {
		return nil, err
	}
	hc = client

	tfs, err := initializeTemplates()
	if err != nil {
		return nil, err
	}
	templatefs = tfs

	router := setupRouter()

	return router, nil
}

func setupRouter() http.Handler {
	// Create mux router
	r := mux.NewRouter()
	// Routes and handlers
	r.HandleFunc("/", ShowHellos)
	r.HandleFunc("/form", ShowForm)
	r.HandleFunc("/submit", FormHandler)

	return r
}

/*TODO: see if this should be moved into main or cmd or kept here
func configureBackendAddress() {
	// Configure ip/hostname and port for backend
	backendName := os.Getenv("BACKEND_NAME")
	if backendName == "" {
		backendName = "localhost"
	}
	backendPort := os.Getenv("BACKEND_PORT")
	if backendPort == "" {
		backendPort = "15200"
	}
	backendAddr := backendName + ":" + backendPort
}
*/

func initializeTemplates() (fs.FS, error) {
	// Set up templates
	tfs, err := fs.Sub(templates, "templates")
	if err != nil {
		return nil, err
	}
	return tfs, nil
}

func createGRPCClientV1(backendAddr string) (pb.HelloServiceClient, error) {
	// Create gRPC API client
	conn, err := grpc.Dial(backendAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	helloClient := pb.NewHelloServiceClient(conn)

	return helloClient, nil
}

// Form submission handler
func FormHandler(w http.ResponseWriter, r *http.Request) {
	// Reject any GET requests
	if r.Method == "GET" {
		// TODO: make error a const and return that error json-serialized
		w.Write([]byte("this a form submission endpoint, wtf u doin fam\n"))
		return
	}

	// Handle the form being submitted
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		// If custom word was entered, return custom hello otherwise return the default
		if (r.Form["word"] != nil) && (len(r.Form["word"][0]) > 0) {
			customWord := r.Form["word"][0]
			_, err := hc.SayCustomHello(context.Background(), &pb.HelloRequest{CustomWord: customWord})
			if err != nil {
				http.Error(w, err.Error(), 500)
			}
		} else {
			_, err := hc.SayHello(context.Background(), &pb.Empty{})
			if err != nil {
				http.Error(w, err.Error(), 500)
			}
		}
		// Send the user back to the main UI to see all hellos
		// TODO: hardcoded /ui endpoint
		http.Redirect(w, r, "/ui", http.StatusMovedPermanently)
		return
	}
}

// Handler for showing all hellos
func ShowHellos(w http.ResponseWriter, _ *http.Request) {
	// Call the backend to get the hellos
	h, err := hc.ShowAllHellos(context.Background(), &pb.Empty{})
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	// Parse the template
	tfs, err := template.ParseFS(templatefs, "main.html", "list.html")
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	// Perform the substitution
	err = tfs.ExecuteTemplate(w, "main", h)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

// Handler to show the form
func ShowForm(w http.ResponseWriter, _ *http.Request) {
	// Parse the template
	tfs, err := template.ParseFS(templatefs, "main.html", "form.html")
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	// Perform the substitution
	err = tfs.ExecuteTemplate(w, "main", nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}
