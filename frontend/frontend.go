package frontend

import (
	"net/http"

	frontv1 "gitlab.com/insanitywholesale/go-grpc-microservice-template/frontend/v1"
	frontv2 "gitlab.com/insanitywholesale/go-grpc-microservice-template/frontend/v2"
)

func CreateFrontendHandler(grpcAddr string) (http.Handler, error) {
	uiV1, err := frontv1.CreateFrontendHandlerV1(grpcAddr)
	if err != nil {
		return nil, err
	}
	uiV2, err := frontv2.CreateFrontendHandlerV2(grpcAddr)
	if err != nil {
		return nil, err
	}

	mux := http.NewServeMux()
	mux.Handle("/ui/v1/", http.StripPrefix("/ui/v1", uiV1))
	mux.Handle("/ui/v2/", http.StripPrefix("/ui/v2", uiV2))

	mux.Handle("/ui/", http.StripPrefix("/ui", uiV2))

	return mux, nil
}
