# Notes
Collection of notes about the project

## Upgrade from buf v1beta1 to v1
It was really problematic and it ended up being just a `buf beta migrate-v1beta1` away.
The [issue I opened at johanbrandhorst/grpc-gateway-boilerplate](https://github.com/johanbrandhorst/grpc-gateway-boilerplate/issues/39) has a good description.
Additionally, there is a branch where I did a similar process to what the `buf` command did but manually, [commit is here](https://gitlab.com/insanitywholesale/go-grpc-microservice-template/-/commit/1b590ad48ce51fc14db330d443a2b6aa71e7d191).
