package mysql

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql" // mysql driver for database/sql
	models "gitlab.com/insanitywholesale/go-grpc-microservice-template/models/v1"
	pb "gitlab.com/insanitywholesale/go-grpc-microservice-template/proto/v1"
)

type mysqlRepo struct {
	client   *sql.DB
	mysqlURL string
}

func newMysqlClient(url string) (*sql.DB, error) {
	client, err := sql.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	err = client.Ping()
	if err != nil {
		return nil, err
	}
	_, err = client.Exec(createHellosTableQuery)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewMysqlRepo(url string) (models.HelloRepo, error) {
	mysqlclient, err := newMysqlClient(url)
	if err != nil {
		return nil, err
	}
	repo := &mysqlRepo{
		mysqlURL: url,
		client:   mysqlclient,
	}
	return repo, nil
}

func (r *mysqlRepo) StoreHello(hr *pb.HelloResponse) error {
	var id uint32
	_, err := r.client.Exec(helloInsertQuery, hr.HelloWord)
	if err != nil {
		return err
	}
	err = r.client.QueryRow(lastInsertIDQuery).Scan(&id)
	if err != nil {
		return err
	}
	hr.Id = id
	return nil
}
