package mysql

var createHellosTableQuery = `CREATE TABLE if not exists Hellos (
	hello_id INTEGER AUTO_INCREMENT NOT NULL,
	hello_word VARCHAR(100),
	PRIMARY KEY (hello_id)
);`

var lastInsertIDQuery = `SELECT LAST_INSERT_ID();`

var helloInsertQuery = `INSERT INTO Hellos (
	hello_word
) VALUES (?);`
