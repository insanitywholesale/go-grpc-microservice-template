# TODOs
Things left to be done before the project is in an okay state

## Structure
Consider modularizing the project further

### Service/Logic layer
A layer that handles the logic so it's decoupled from the grpc implementation

#### Options
The ways to achieve it that I can think of are
- a struct with methods that implement the logic and will also hold the db which gets passed to it from the grpc server struct
- a set of functions that implement parts of the logic and the grpc layer will handle db interaction
- a set of functions that take the db as an argument and will be called from the grpc layer

### Logger
Currently the logger is hard-coded to be the `log` package from the standard library, should probably change that

## Documentation
Since this is a showcase of a template there should be good documentation

### Tools
This tool seems *really* interesting [protoc-gen-doc](https://github.com/pseudomuto/protoc-gen-doc)

### Code comments
Add more comments in code to explain the rationale behind decisions as well as the not immediately-obvious parts

### Architecture
By far the most complicated thing here is the internal structure
- add `ARCHITECTURE.md` to explain it
- add graphviz graph to explain it

### Environment variables
Create a table of environment variables used throughout the project with a description and default values

### Working with the project
It's not immediately obvious how to use surrounding tooling
- document how to use pprof, fgprof
- document how to use buf

## CI files
The more CI, the better
- add `Jenkinsfile`
- add `.travis.yml`
- add `sagefile.go` [link here cause it's hard to search](https://github.com/einride/sage)
- add whatever Tekton has
- add renovatebot config
- add dependabot config
- add earthfile [link here cause search engines freak out a bit](https://earthly.dev/)

## Deployment files
Need a way to run the application
- add `docker-compose.yml` for easier local testing with a real db
- add raw kubernetes yaml
- add kustomize yaml to demonstrate testing/prod

## Tests
A lot of things don't have tests
- non-mock databases. can try [dockertest](https://github.com/ory/dockertest) and [testcontainers](https://github.com/testcontainers/testcontainers-go) for this
- models/interfaces (no idea how to test these)
- could use [gotests](https://github.com/cweill/gotests) to generate boilerplate for tests

## Optimization
Go gotta go brrr

### Struct alignment
Align struct fields using [fieldalignment](https://pkg.go.dev/golang.org/x/tools/go/analysis/passes/fieldalignment) (main.go file is [in this repo](https://cs.opensource.google/go/x/tools/+/refs/tags/v0.1.12:go/analysis/passes/fieldalignment/cmd/fieldalignment/main.go))

## Errors
Wrap errors to add details at each step instead of doing `return nil, err`

## protoc
It was removed because it didn't work with grpc-gateway v2, investigate why and look into re-introducing it if it works

## Functionality
Functionality is not yet complete

### Add default docs handler
Currently `/api/docs/` results in error, it should point to current version docs

### Replace Empty gRPC message
If we want to accept something other than Empty there will be problems in the future so to avoid this replace it with a HelloFilter message or something

### Additional version demo
Add later API versions to demonstrate that fields can be added but breaking changes need a full API version change

### Rethink REST api versioning
Might want to actually namespace this but it works somewhat okay

### Repo
Adjust repos to store and retrieve all info for HelloRequest/HelloResponse

#### Extra repo options
The more, the merrier
- [sq](https://github.com/bokwoon95/sq)
- [protoc-gen-gorm](https://github.com/infobloxopen/protoc-gen-gorm)
- [sqlc](https://github.com/kyleconroy/sqlc) for generating typed queries (supports postgres, mysql and sqlite)
- tons more I'd like to add but we'll see

#### Migration support
Gitlab uses [journey](https://github.com/db-journey/journey), check it out maybe

### Multiplexing
Add the option to run both grpc and rest on the same port but explain why it's dumb

### Standalone server
Add function CreateListenerFromPortAndAddress in `utils` to be able to create a listener from a user-supplied address.
Also add an env var or two (`HELLO_GRPC_ADDRESS`, `HELLO_REST_ADDRESS`) and maybe a CLI too to run with that user-supplied address

### Frontend
Look at frontend implementation on [lister](https://gitlab.com/insanitywholesale/lister) for optionally split frontend

#### Starting
Two approaches:
- embed frontend by default and have a `cmd/standalone` for running it on its own
- exclude frontend by default and have a `cmd/frontend` or `cmd/omnibus` for running it embedded

Also we need to figure out how it will be mapped to an endpoint.
Both the regular main function and the main inside `cmd` need to be able to map the handler to an endpoint.

#### API versioning
Add versioned endpoints with a default that hits the latest one

#### Custom backend address
Currently the backend address is hardcoded, add a way to change it to a user-supplied thing

### CLI
Might be able to auto-generate a CLI tool with [aip cli go](https://github.com/einride/aip-cli-go)

### Refactor to be able to use unix sockets
Didn't think about using unix sockets instead of tcp sockets
Refactor util methods and listen address code to support it

### Change OpenAPI code to be like frontend
Main doesn't need to know how many api versions there are
Figured it out with frontend so port the changes over to OpenAPI

### Config
Have a centralized config struct somewhere
Might be worth it to look into viper too

### Configurable timeouts
Both in `main.go` and `cmd/frontend` there should be env vars for the timeouts

### Profiling

#### HTTP endpoints
Check if profiler endpoints work

#### Add autopprof
After finding [autopprof](https://github.com/daangn/autopprof) I think it's worth adding it
