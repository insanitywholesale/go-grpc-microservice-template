.PHONY: checkformat getdeps protos gorelease

checkformat:
	diff -u <(echo -n) <(gofmt -d ./)

protos:
	buf generate --timeout=5m30s

gorelease:
	go install -v github.com/goreleaser/goreleaser@latest
	goreleaser --snapshot --skip-publish --rm-dist
